from __future__ import print_function

import logging
import numpy as np
from optparse import OptionParser
import sys
from time import time
import pylab as pl
import string

import inspect
import unicodedata
import sys

from sklearn.decomposition import SparsePCA
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import NearestCentroid
from sklearn.utils.extmath import density
from sklearn import metrics
import re
import cPickle as pickle
import codecs
# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')


# parse commandline arguments
op = OptionParser()
op.add_option("--report",
              action="store_true", dest="print_report",
              help="Print a detailed classification report.")
op.add_option("--chi2_select",
              action="store", type="int", dest="select_chi2",
              help="Select some number of features using a chi-squared test")
op.add_option("--confusion_matrix",
              action="store_true", dest="print_cm",
              help="Print the confusion matrix.")
op.add_option("--top10",
              action="store_true", dest="print_top10",
              help="Print ten most discriminative terms per class"
                   " for every classifier.")
op.add_option("--all_categories",
              action="store_true", dest="all_categories",
              help="Whether to use all categories or not.")
op.add_option("--use_hashing",
              action="store_true",
              help="Use a hashing vectorizer.")
op.add_option("--n_features",
              action="store", type=int, default=2 ** 16,
              help="n_features when using the hashing vectorizer.")
op.add_option("--filtered",
              action="store_true",
              help="Remove newsgroup information that is easily overfit: "
                   "headers, signatures, and quoting.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

print(__doc__)
op.print_help()
print()

text1 = codecs.open('text1','wb','utf-8')
huge_list = []

def get_class_that_defined_method(meth):
	for cls in inspect.getmro(meth.im_class):
		if meth.__name__ in cls.__dict__: return cls	
	return None

word_class_dict = pickle.load(open('/home/devika/Desktop/AML_project/trunk/word_class_kmeans_hier_500_traindata_100mb.p','rb'))
cluster_no=500

print("Loading 20 newsgroups dataset for categories:")

remove = ('headers', 'footers', 'quotes')

data_train = fetch_20newsgroups(subset='train', categories=None,
                                shuffle=True, random_state=42,
                                remove=remove)

data_test = fetch_20newsgroups(subset='test', categories=None,
                               shuffle=True, random_state=42,
                               remove=remove)
print('data loaded')

categories = data_train.target_names 

print(categories)

def size_mb(docs):
    return sum(len(s.encode('utf-8')) for s in docs) / 1e6

data_train_size_mb = size_mb(data_train.data)
data_test_size_mb = size_mb(data_test.data)

print("%d documents - %0.3fMB (training set)" % (
    len(data_train.data), data_train_size_mb))
print("%d documents - %0.3fMB (test set)" % (
    len(data_test.data), data_test_size_mb))
print("%d categories" % len(categories))
print()

# split a training set and a test set
y_train, y_test = data_train.target, data_test.target

feature_names = None


tbl = dict.fromkeys(i for i in xrange(sys.maxunicode) if unicodedata.category(unichr(i)).startswith('P'))
exclude = set(string.punctuation)
exclude.add('=')
X_train = []
#print(translate.im_class)
regex = re.compile('[%s]' % re.escape(string.punctuation))
count=1
for text in data_train.data:
	traindata=text.replace("\n",'')
	traindata=traindata.strip()
	#traindata = traindata.decode('utf-8', 'ignore')
	#unicodedata.normalize('NFKD', traindata).encode('ascii','ignore')
	#print(count)
	count=count+1
	out = regex.sub(' ', traindata)
	##out = ''.join(ch for ch in traindata if ch not in exclude)
	#out = traindata.translate(string.maketrans("",""), string.punctuation)
	#out = traindata.translate(tbl)
	#out = out.decode('utf-8', 'ignore')
	out = out.split()
	features=[0.0]*cluster_no
	for word in out :
		#print(word.lower())
		#huge_list.append(word.lower()+' ')
		if word.lower() in word_class_dict:
			cluster = word_class_dict[word.lower()]
			#print(cluster)
			features[int(cluster[0])]=features[int(cluster[0])]+1
	'''	
	if len(out)>0:
		for entry in features:
			entry=entry/len(out)'''
	X_train.append(features)
print("--------------------------------------------")
print("printing x_train")
#huge_string = "".join(item for item in huge_list)
#text1.write(huge_string)
text1.close()
#spca=SparsePCA(n_components=20, alpha=1, ridge_alpha=0.01, max_iter=1000, tol=1e-08, method='lars', n_jobs=1, U_init=None, V_init=None, verbose=False, random_state=None)


#print(X_train)
X_test=[]
count=1
for text in data_test.data:
	testdata=text.replace("\n",'')
	testdata=testdata.strip()
	#traindata = traindata.decode('utf-8', 'ignore')
	#unicodedata.normalize('NFKD', traindata).encode('ascii','ignore')
	#print(count)
	count=count+1
	out = regex.sub(' ', testdata)
	##out = ''.join(ch for ch in traindata if ch not in exclude)
	#out = traindata.translate(string.maketrans("",""), string.punctuation)
	#out = traindata.translate(tbl)
	#out = out.decode('utf-8', 'ignore')
	out = out.split()
	features=[0.0]*cluster_no
	for word in out :
		#print(word.lower())
		if word.lower() in word_class_dict:
			cluster = word_class_dict[word.lower()]
			#print(cluster)
			features[int(cluster[0])]=features[int(cluster[0])]+1
	'''	
	if len(out)>0:
		for entry in features:
			entry=entry/len(out)'''
	X_test.append(features)
print("--------------------------------------------")
print("printing x_test")

#print(X_train)
#except Exception,e: 
	#print(str(e))
		#print(type(traindata))
	#out = traindata.translate(string.maketrans("",""), string.punctuation)
#	pass
#print len(data_test.data)

pickle.dump(X_train, open('X_train.p','wb'))
pickle.dump(X_test, open('X_test.p','wb'))

X_train = np.array(X_train)
X_test = np.array(X_test)

#X_train = spca.fit_transform(X_train)
#X_test = spca.fit_transform(X_test)
###############################################################################
# Benchmark classifiers
def benchmark(clf):
    print('_' * 80)
    print("Training: ")
    print(clf)
    t0 = time()
    clf.fit(X_train, y_train)
    train_time = time() - t0
    print("train time: %0.3fs" % train_time)

    t0 = time()
    pred = clf.predict(X_test)
    test_time = time() - t0
    print("test time:  %0.3fs" % test_time)

    score = metrics.f1_score(y_test, pred)
    print("f1-score:   %0.3f" % score)

    if hasattr(clf, 'coef_'):
        print("dimensionality: %d" % clf.coef_.shape[1])
        print("density: %f" % density(clf.coef_))

        if opts.print_top10 and feature_names is not None:
            print("top 10 keywords per class:")
            for i, category in enumerate(categories):
                top10 = np.argsort(clf.coef_[i])[-10:]
                print(trim("%s: %s"
                      % (category, " ".join(feature_names[top10]))))
        print()

    if opts.print_report:
        print("classification report:")
        print(metrics.classification_report(y_test, pred,
                                            target_names=categories))

    if opts.print_cm:
        print("confusion matrix:")
        print(metrics.confusion_matrix(y_test, pred))

    print()
    clf_descr = str(clf).split('(')[0]
    return clf_descr, score, train_time, test_time


results = []
for clf, name in (
        (RidgeClassifier(tol=1e-2, solver="lsqr"), "Ridge Classifier"),
        (Perceptron(n_iter=50), "Perceptron"),
        (PassiveAggressiveClassifier(n_iter=50), "Passive-Aggressive"),
        (KNeighborsClassifier(n_neighbors=10), "kNN")):
    print('=' * 80)
    print(name)
    results.append(benchmark(clf))

for penalty in ["l2", "l1"]:
    print('=' * 80)
    print("%s penalty" % penalty.upper())
    # Train Liblinear model
    results.append(benchmark(LinearSVC(loss='l2', penalty=penalty,
                                            dual=False, tol=1e-3)))

    # Train SGD model
    results.append(benchmark(SGDClassifier(alpha=.0001, n_iter=50,
                                           penalty=penalty)))

# Train SGD with Elastic Net penalty
print('=' * 80)
print("Elastic-Net penalty")
results.append(benchmark(SGDClassifier(alpha=.0001, n_iter=50,
                                       penalty="elasticnet")))

# Train NearestCentroid without threshold
print('=' * 80)
print("NearestCentroid (aka Rocchio classifier)")
results.append(benchmark(NearestCentroid()))

# Train sparse Naive Bayes classifiers
print('=' * 80)
print("Naive Bayes")
results.append(benchmark(MultinomialNB(alpha=.01)))
results.append(benchmark(BernoulliNB(alpha=.01)))


class L1LinearSVC(LinearSVC):

    def fit(self, X, y):
        # The smaller C, the stronger the regularization.
        # The more regularization, the more sparsity.
        self.transformer_ = LinearSVC(penalty="l1",
                                      dual=False, tol=1e-3)
        X = self.transformer_.fit_transform(X, y)
        return LinearSVC.fit(self, X, y)

    def predict(self, X):
        X = self.transformer_.transform(X)
        return LinearSVC.predict(self, X)

print('=' * 80)
print("LinearSVC with L1-based feature selection")
results.append(benchmark(L1LinearSVC()))


# make some plots

indices = np.arange(len(results))

results = [[x[i] for x in results] for i in range(4)]

clf_names, score, training_time, test_time = results
training_time = np.array(training_time) / np.max(training_time)
test_time = np.array(test_time) / np.max(test_time)

pl.figure(figsize=(12,8))
pl.title("Score")
pl.barh(indices, score, .2, label="score", color='r')
pl.barh(indices + .3, training_time, .2, label="training time", color='g')
pl.barh(indices + .6, test_time, .2, label="test time", color='b')
pl.yticks(())
pl.legend(loc='best')
pl.subplots_adjust(left=.25)
pl.subplots_adjust(top=.95)
pl.subplots_adjust(bottom=.05)

for i, c in zip(indices, clf_names):
    pl.text(-.3, i, c)

pl.show()
