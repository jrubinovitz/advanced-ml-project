import numpy
import pylab
import sys
from sklearn.cluster import AffinityPropagation 

from csv_array import make_array

data = make_array()
X = data[0]
words = data[1]
model = AffinityPropagation(damping=0.5, max_iter=200, convergence_iter=15, copy=False, preference=None, affinity='euclidean', verbose=False).fit(X)
labels = model.labels_
print 'number of labels'
print len(labels)
print 'number of words'
print len(words)

filename = "ap_k100.txt"
outfile = open(filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]))

outfile.close()
