import numpy
import pylab
import sys
from sklearn.cluster import MeanShift 
from csv_array import make_array

data = make_array()
X = data[0]
words = data[1]

model = MeanShift(bandwidth=None, seeds=None, bin_seeding=False, min_bin_freq=10, cluster_all=True).fit(X)
labels = model.labels_
print 'number of labels'
print len(labels)
print 'number of words'
print len(words)

filename = "ms_k100.txt"
outfile = open(filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]))

outfile.close()
