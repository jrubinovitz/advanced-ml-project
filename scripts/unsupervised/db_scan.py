import numpy
import sys
from sklearn.cluster import DBSCAN 
from csv_array import make_array

data = make_array()
X = data[0]
words = data[1]

model = DBSCAN(eps=0.5, min_samples=5, metric='euclidean', algorithm='auto', leaf_size=100, p=None, random_state=None).fit(X)
labels = model.labels_
print 'number of labels'
print len(labels)
print 'number of words'
print len(words)


filename = "dbscan_k100.txt"
outfile = open(filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]) + '\n')

outfile.close()
