from sklearn.pipeline import Pipeline
from sklearn.neighbors import NearestNeighbors
import numpy
import pylab
import sys
from sklearn.cluster import Ward
from sklearn.svm import LinearSVC

a = open('../vectors-text.bin', 'r')
read_file = a.read()
array = read_file.split(' ')
array = array[:len(array)-1]
# remove numbers from front
array = array[1:]
word_1 = array[0].split('\n')[1]
array[0] = word_1

words = []
rows = []
row = []
for x in array:
    try:
        y = float(x)
        row.append(y)
    except:
        words.append(x)
        if len(row) > 1:
            rows.append(row)
            row = []

words = words[1:]
X = numpy.array(rows[:20000])
lengths = [len(line) for line in X]
print numpy.unique(lengths)

if len(numpy.unique(lengths)) > 1:
    removes = []
    for i in range(len(lengths)):
        if lengths[i] != 200:
            print i
            print len(rows[i])
            removes.append(i)

    j = 0
    for index in removes:
        del words[index-j]
        del rows[index-j]
        j = j + 1

    X = numpy.array(rows)

lengths = [len(line) for line in X]
print numpy.unique(lengths)

"""
ward = Ward(n_clusters=10, copy=False).fit(X)
labels = ward.labels_
print 'number of labels'
print len(labels)
print 'number of words'
print len(words)
"""

clf = Pipeline([
      ('feature_selection', LinearSVC(penalty="l1",dual=False)),
        ('classification', Ward())
        ])
clf.fit(X)
