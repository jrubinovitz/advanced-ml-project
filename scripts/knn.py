from sklearn.neighbors import NearestNeighbors
import numpy
import pylab
from sklearn import preprocessing
import sys

a = open('../vectors-text.bin', 'r')
read_file = a.read()
array = read_file.split(' ')
array = array[:len(array)-1]
# remove numbers from front
array = array[1:]
word_1 = array[0].split('\n')[1]
array[0] = word_1

words = []
rows = []
row = []
for x in array:
    try:
        y = float(x)
        row.append(y)
    except:
        words.append(x)
        if len(row) > 1:
            rows.append(row)
            row = []

words = words[1:]
"""
print words[0]
print rows[0]
print 'length of row 1'
print len(rows[0])
print 'number of rows'
print len(rows)
print 'number of words'
print len(words)
"""
X = numpy.array(rows)
lengths = [len(line) for line in X]
print numpy.unique(lengths)


removes = []
for i in range(len(lengths)):
    if lengths[i] == 401:
        print i
        print len(rows[i])
        removes.append(i)

i = 0
for index in removes:
    del words[index-i]
    del rows[index-i]
    i = i + 1

X = numpy.array(rows)

lengths = [len(line) for line in X]
#print numpy.unique(lengths)


nbrs = NearestNeighbors(n_neighbors=10, algorithm='ball_tree').fit(X)
print nbrs
