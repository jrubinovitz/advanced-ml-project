import numpy
import pylab
import sys
from sklearn.cluster import AffinityPropagation 

a = open('../../vectors-text.bin', 'r')
read_file = a.read()
array = read_file.split(' ')
array = array[:len(array)-1][:30000]
# remove numbers from front
array = array[1:]
word_1 = array[0].split('\n')[1]
array[0] = word_1

words = []
rows = []
row = []
for x in array:
    try:
        y = float(x)
        row.append(y)
    except:
        words.append(x)
        if len(row) > 1:
            rows.append(row)
            row = []

words = words[1:]
print 'rows:'
print len(rows)
X = numpy.array(rows)
lengths = [len(line) for line in X]

if len(numpy.unique(lengths)) > 1:
    removes = []
    for i in range(len(lengths)):
        if lengths[i] != 200:
            removes.append(i)

    i = 0
    for index in removes:
        del words[index-i]
        del rows[index-i]
        i = i + 1

    X = numpy.array(rows)

lengths = [len(line) for line in X]

model = AffinityPropagation(damping=0.5, max_iter=200, convergence_iter=15, copy=False, preference=None, affinity='euclidean', verbose=False).fit(X)
labels = model.labels_
print 'number of labels'
print len(labels)
print 'number of words'
print len(words)

filename = "ap_k100.txt"
outfile = open(filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]))

outfile.close()
