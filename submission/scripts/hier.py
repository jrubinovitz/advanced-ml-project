import numpy
import sys
from sklearn.cluster import Ward
from csv_array import make_array
import cPickle as pickle

args = sys.argv

if len(args) > 1:
        k = int(args[1])
        print 'k is %i'
        print k

else:   
        k = 50

data = make_array()
X = data[0]
words = data[1]

ward = Ward(n_clusters=k, copy=False).fit(X)
labels = ward.labels_

label_filename = "ward_"+str(k)+".txt"
pickle_file = open("ward_"+str(k)+".p", "wb")
outfile = open(label_filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]) +'\n')

outfile.close()

pickle.dump(ward, pickle_file)

