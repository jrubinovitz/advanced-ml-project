from sklearn import metrics
import cPickle as pickle
import numpy as np

labels_true=[]
labels_pred=[]

dict1 = pickle.load(open('/home/devika/Desktop/AML_project/trunk/word_class_ward_100_5mb.p','rb'))
dict2 = pickle.load(open('/home/devika/Desktop/AML_project/trunk/word_class_spec_100_5mb.p','rb'))

for key in dict1:
	labels_true.append(dict1[key][0])
	labels_pred.append(dict2[key][0])

print labels_true
print labels_pred

labels_true = np.array(labels_true)
labels_pred = np.array(labels_pred)
#labels_true = [0, 0, 0, 1, 1, 1]
#labels_pred = [0, 0, 1, 1, 2, 2]

print "Rand Index is :"
print metrics.adjusted_rand_score(labels_true, labels_pred)  

print "Mutual Information is :"
print metrics.adjusted_mutual_info_score(labels_true, labels_pred)

print "Homogeneity, Completeness and V measure (note that these are not normalised w.r.t random labelling) :"
print metrics.homogeneity_completeness_v_measure(labels_true, labels_pred)
