import numpy
import sys
import csv

def make_array():
    words = []
    rows = []
    with open('../../vectors_10mb.csv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            word = row[0]
            try:
                nums = [float (x) for x in row[1:]]
                words.append(word)
                rows.append(nums)
            except:
                pass

    print len(rows)
    print len(words)
    X = numpy.array(rows)
    lengths = [len(line) for line in X]

    if len(numpy.unique(lengths)) > 1:
        removes = []
        for i in range(len(lengths)):
            print i
            if lengths[i] != 200:
                removes.append(i)

        i = 0
        for index in removes:
            del words[index-i]
            del rows[index-i]
            i = i + 1

        X = numpy.array(rows)
    return [X, words]
