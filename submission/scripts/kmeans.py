import numpy
import sys
from sklearn.cluster import KMeans 
from csv_array import make_array
import cPickle as pickle

args = sys.argv

if len(args) > 1:
        k = int(args[1])
        print 'k is %i'
        print k

else:   
        k = 5000

data = make_array()
X = data[0]
words = data[1]

model = KMeans(n_clusters=k).fit(X)
labels = model.labels_
centers = model.cluster_centers_
label_filename = "labels_model_"+str(k)+".txt"
center_filename = "centers_model_"+str(k)+".txt"
pickle_file = open("kmeans_10mb_k5000.p", "wb")
labels_outfile = open(label_filename, "w")
centers_outfile=open(center_filename,"w")

for i in range(len(words)):
    labels_outfile.write(words[i] + ',' +  str(labels[i]) +'\n')
 #   centers_outfile.write(words[i] + ',' +  str(centers[i]) +'\n')

for center in centers:
    center_outfile.write(center)

labels_outfile.close()
centers_outfile.close()

pickle.dump(model, pickle_file)

