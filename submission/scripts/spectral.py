import numpy
import sys
from sklearn.cluster import SpectralClustering 
from csv_array import make_array
import cPickle as pickle

args = sys.argv

if len(args) > 1:
        k = int(args[1])
        print 'k ='
        print k

else:   
        k = 50


data = make_array()
X = data[0]
words = data[1]
model = SpectralClustering(n_clusters=k, eigen_solver=None, random_state=None, n_init=10, gamma=1.0, affinity='rbf', n_neighbors=10, k=None, eigen_tol=0.0, assign_labels='kmeans', mode=None, degree=3, coef0=1, kernel_params=None).fit(X)
labels = model.labels_


label_filename = "spec_"+str(k)+".txt"
pickle_file = open("spec_"+str(k)+".p", "wb")
outfile = open(label_filename, "w")

for i in range(len(words)):
    outfile.write(words[i] + ',' +  str(labels[i]) +'\n')

outfile.close()

labels_set = set(labels)
print 'number of clusters'
print len(labels_set)

pickle.dump(model, pickle_file)

