None
Usage: try_loading_data.py [options]

Options:
  -h, --help            show this help message and exit
  --report              Print a detailed classification report.
  --chi2_select=SELECT_CHI2
                        Select some number of features using a chi-squared
                        test
  --confusion_matrix    Print the confusion matrix.
  --top10               Print ten most discriminative terms per class for
                        every classifier.
  --all_categories      Whether to use all categories or not.
  --use_hashing         Use a hashing vectorizer.
  --n_features=N_FEATURES
                        n_features when using the hashing vectorizer.
  --filtered            Remove newsgroup information that is easily overfit:
                        headers, signatures, and quoting.

Loading 20 newsgroups dataset for categories:
data loaded
['alt.atheism', 'comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware', 'comp.windows.x', 'misc.forsale', 'rec.autos', 'rec.motorcycles', 'rec.sport.baseball', 'rec.sport.hockey', 'sci.crypt', 'sci.electronics', 'sci.med', 'sci.space', 'soc.religion.christian', 'talk.politics.guns', 'talk.politics.mideast', 'talk.politics.misc', 'talk.religion.misc']
11314 documents - 13.782MB (training set)
7532 documents - 8.262MB (test set)
20 categories

--------------------------------------------
printing x_train
--------------------------------------------
printing x_test
================================================================================
Ridge Classifier
________________________________________________________________________________
Training: 
RidgeClassifier(alpha=1.0, class_weight=None, copy_X=True, fit_intercept=True,
        max_iter=None, normalize=False, solver=lsqr, tol=0.01)
train time: 0.182s
test time:  0.011s
f1-score:   0.054
dimensionality: 50
density: 0.980000


================================================================================
Perceptron
________________________________________________________________________________
Training: 
Perceptron(alpha=0.0001, class_weight=None, eta0=1.0, fit_intercept=True,
      n_iter=50, n_jobs=1, penalty=None, random_state=0, shuffle=False,
      verbose=0, warm_start=False)
train time: 1.147s
test time:  0.012s
f1-score:   0.158
dimensionality: 50
density: 0.978000


================================================================================
Passive-Aggressive
________________________________________________________________________________
Training: 
PassiveAggressiveClassifier(C=1.0, fit_intercept=True, loss=hinge, n_iter=50,
              n_jobs=1, random_state=None, shuffle=False, verbose=0,
              warm_start=False)
train time: 1.877s
test time:  0.011s
f1-score:   0.027
dimensionality: 50
density: 0.980000


================================================================================
kNN
________________________________________________________________________________
Training: 
KNeighborsClassifier(algorithm=auto, leaf_size=30, metric=minkowski,
           n_neighbors=10, p=2, weights=uniform)
train time: 0.101s
test time:  3.780s
f1-score:   0.141

================================================================================
L2 penalty
________________________________________________________________________________
Training: 
LinearSVC(C=1.0, class_weight=None, dual=False, fit_intercept=True,
     intercept_scaling=1, loss=l2, multi_class=ovr, penalty=l2,
     random_state=None, tol=0.001, verbose=0)
train time: 14.191s
test time:  0.012s
f1-score:   0.237
dimensionality: 50
density: 0.980000


________________________________________________________________________________
Training: 
SGDClassifier(alpha=0.0001, class_weight=None, epsilon=0.1, eta0=0.0,
       fit_intercept=True, l1_ratio=0.15, learning_rate=optimal,
       loss=hinge, n_iter=50, n_jobs=1, penalty=l2, power_t=0.5,
       random_state=None, rho=None, shuffle=False, verbose=0,
       warm_start=False)
train time: 1.185s
test time:  0.011s
f1-score:   0.176
dimensionality: 50
density: 0.980000


================================================================================
L1 penalty
________________________________________________________________________________
Training: 
LinearSVC(C=1.0, class_weight=None, dual=False, fit_intercept=True,
     intercept_scaling=1, loss=l2, multi_class=ovr, penalty=l1,
     random_state=None, tol=0.001, verbose=0)
train time: 24.454s
test time:  0.012s
f1-score:   0.237
dimensionality: 50
density: 0.976000


________________________________________________________________________________
Training: 
SGDClassifier(alpha=0.0001, class_weight=None, epsilon=0.1, eta0=0.0,
       fit_intercept=True, l1_ratio=0.15, learning_rate=optimal,
       loss=hinge, n_iter=50, n_jobs=1, penalty=l1, power_t=0.5,
       random_state=None, rho=None, shuffle=False, verbose=0,
       warm_start=False)
train time: 3.437s
test time:  0.011s
f1-score:   0.184
dimensionality: 50
density: 0.959000


================================================================================
Elastic-Net penalty
________________________________________________________________________________
Training: 
SGDClassifier(alpha=0.0001, class_weight=None, epsilon=0.1, eta0=0.0,
       fit_intercept=True, l1_ratio=0.15, learning_rate=optimal,
       loss=hinge, n_iter=50, n_jobs=1, penalty=elasticnet, power_t=0.5,
       random_state=None, rho=None, shuffle=False, verbose=0,
       warm_start=False)
train time: 3.992s
test time:  0.011s
f1-score:   0.195
dimensionality: 50
density: 0.923000


================================================================================
NearestCentroid (aka Rocchio classifier)
________________________________________________________________________________
Training: 
NearestCentroid(metric=euclidean, shrink_threshold=None)
train time: 0.011s
test time:  0.015s
f1-score:   0.068

================================================================================
Naive Bayes
________________________________________________________________________________
Training: 
MultinomialNB(alpha=0.01, class_prior=None, fit_prior=True)
train time: 0.067s
test time:  0.012s
f1-score:   0.221
dimensionality: 50
density: 1.000000


________________________________________________________________________________
Training: 
BernoulliNB(alpha=0.01, binarize=0.0, class_prior=None, fit_prior=True)
train time: 0.098s
test time:  0.017s
f1-score:   0.144
dimensionality: 50
density: 1.000000


================================================================================
LinearSVC with L1-based feature selection
________________________________________________________________________________
Training: 
L1LinearSVC(C=1.0, class_weight=None, dual=True, fit_intercept=True,
      intercept_scaling=1, loss=l2, multi_class=ovr, penalty=l2,
      random_state=None, tol=0.0001, verbose=0)
train time: 89.285s
test time:  0.020s
f1-score:   0.202
dimensionality: 49
density: 1.000000


