\documentclass{article} % For LaTeX2e
\usepackage{nips12submit_e,times}
\nipsfinalcopy % Uncomment for camera-ready version
%\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}

\usepackage{graphicx}
\graphicspath{ {C:\Users\Devika\Pictures} }
 \DeclareGraphicsExtensions{.pdf,.jpeg,.png,.jpg}




\newcommand{\B}[1]{{\bf #1}}
\newcommand{\Sc}[1]{{\mathcal{#1}}}
\newcommand{\R}[1]{{\rm #1}}
\newcommand{\mB}[1]{{\mathbb{#1}}}
%%%%%%%%%%%%%%%%%%%%%%%%%
% Commands copied from hart.sty
\newcommand{\bB}{\bf{B}}\newcommand{\bN}{\bf{N}}
% \newcommand{\B{1}}{\mathbb {1}}
%%%%%%%%%%%%%%%%%%%%%%%%%


% Macros added by Burke
\newcommand{\set}[2]{\left\{#1\,\left\vert\, #2\right.\right\}}
\newcommand{\one}{\bf{1}}
\newcommand{\half}{\frac{1}{2}}
\newcommand{\map}[3]{#1\,:\,#2\rightarrow #3}
\newcommand{\cS}{\mathcal{S}}
\newcommand{\cL}{\mathcal{L}}


\newtheorem{lemma}{Lemma}[section]
%\newtheorem{remark}{Remark}[section]
\newtheorem{remark}[lemma]{Remark}
\newtheorem{theorem}[lemma]{Theorem}
\newtheorem{corollary}[lemma]{Corollary}
\newtheorem{conjecture}[lemma]{Conjecture}
\newtheorem{proposition}[lemma]{Proposition}
\newtheorem{definition}[lemma]{Definition}
\newtheorem{algorithm}[lemma]{A}

\title{Clustering and Classification with Word Vectors}

\author{
Devika Dwivedi\\
Department of Computer Science\\
University of Columbia, New York, NY\\
\texttt{dd2708@columbia.edu} \\
\And
Cindy Long\\
Department of Computer Science\\
University of Columbia, New York, NY\\
\texttt{xl2259@columbia.edu} \\
\And
Jennifer Rubinovitz\\
Department of Computer Science\\
University of Columbia, New York, NY\\
\texttt{jrubinovitz@columbia.edu} \\
}
% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
Word vectors are an up and coming feature in natural language processing. One application of word vectors is topic modeling. The clustering method conventionally used for classification in word vector topic modeling is the k-means algorithm. However, the k-means algorithm is convex and does not perform well on non-convex data sets. One can assume the vector space of these vectors is not convex, so we experimented with clustering methods, spectral clustering and hierarchical clustering, that do perform well on convex sets. The clusters were tested by performing text classification using the clusters. Although the classification results were poor, the results did indicate that given a large enough amount of training data, non-convex clustering methods gain an edge over k-means clustering, thus better capturing the semantic properties of the English language.
\end{abstract}

\vspace{-.2cm}
\section{Introduction}
\label{EGintroduction}
\vspace{-.2cm}

Neural networks can be applied to language modeling by converting words into high dimensional, real valued vectors. Whereas n-gram modeling is discrete, word vector representations can capture new relationships between words. Using the vectors, one can automate the process of generating and extending dictionaries and phrase tables. 
Using distributed representations of words, a linear mapping between vector
spaces of languages can be discovered. One possible application is language translation. When visualized with PCA  vector representations of similar words in different languages were related by a linear transformation. Using word vectors, almost 90\% precision  for translation of words between English and Spanish has been achieved \cite{MikolovLe}. Another application, that we will explore, is topic modeling through the clustering of these vectors.

Google built a tool based on \cite{MikolovChen},  \cite{MikolovLe} and  \cite{MikolovWen}, word2vec, that computes vector representations of words from very large data sets. Word2vec uses the k-means clustering method and two neural nets: the skip-gram and the continuous bag of words. In this paper we examine other methods of clustering and compare their performances to k-means using the Newsgroup data provided with word2vec \cite{Word2vec}. K-means does not perform well on non-convex data sets (we prove the convexity of k-means in section 3.1), so we were motivated to try clustering the vectors using methods that can handle non-convex sets.

In this paper, we explain modern methods of word vector representations and their clustering, and then tell of our own clustering experiments using hierarchical clustering and spectral clustering, and then share our results.

The paper proceeds as follows. In Section 2, we formulate the skip-word and continuous bag of words word vector models used in word2vec. In Section 3, we prove the convexity of k-means and explore spectral and hierarchical clustering methods. In Section 4, we test spectral and hierarchical clustering against the conventional k-means approach.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation of Word Vector Representations 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\vspace{-.2cm}
\section{Estimation of Word Vector Representations}
\label{StateDependence}
\vspace{-.2cm}


Word2vec uses the continuous bag-of-words and skip-gram architectures. The skip-gram model identifies a large number of phrases as individual tokens. This model is called “skip-gram” because these tokens can have words in between (skip) but they must be consecutive. The quality of these phrase vectors were tested using a set of analogical reasoning tasks that contain both words and phrases. This model maximizes the average log probability of a sequence of training words. 
\begin{equation}
\frac{1}{T}\sum_{t=1}^{T} \sum_{-c \leq j \leq c, j \neq 0} logp(w_{t+j}|w_{t})
\end{equation}

 where $w_{j}= w_{1}, w_{2}, …, w_{T}$ is a sequence of training words, and c is the size of the training context. It is more beneficial to use large training data for higher accuracy but computation time increases. 

 The logistic function maps a vector of real values of length $p$ to a vector of length $K$ of value $vw$. The soft-max function is a generalization of the logistic function. 

 \begin{equation}
     p(w_{O}|w_{I}) = \frac{exp(\breve{v}_{w_{o}^{T}})}{\sum_{w=1}^{W} exp(\breve{v}_{w}^{T} v_{wI})}
\end{equation}
 Here, $v_{wi}$ is the input vector and $v_{wo}$ is the output vector, and W is the number of words in the vocabulary. The Hessian of $log(w_{o} | w_{i})$ is proportional to W and because W is often approximately 105-107 terms, this cost of computation is too inefficient for this soft-max function. 

 An alternative is to use hierarchical soft-max function, which can efficiently approximate the soft-max without evaluating the W output nodes in the neural network to obtain the probability distribution. 

\begin{equation}
    p(w|w_{I})=\prod_{j=1}^{L(w)-1} \sigma([n(w,j+1)=ch(n(w,j))] \cdot  \breve{v}_{n(w,j)}^{T}v_{wI})
\end{equation}


The hierarchical soft-max function evaluates $log_{2}W$  nodes by function using a binary tree representation of the output layer with the $W$ words as its leaves. Each node represents the relative probabilities of its child nodes. By using a random walk that assigns probabilities to words computation time is greatly diminished.


The neural network of word2vec has a single hidden layer. The FeedForward Neural Network Language Model (NNLM) consists of input, projection, hidden and output layers. The input layer has N previous words that are encoded using 1-of-V coding, where V is the size of the vocabulary. This model projects the input layer to a projection layer P that has dimensionality N x D, using a shared projection matrix.  The neural net can train continous bag of words and skipgram models.

\subsection{Continuous Bag of Words}
CBOW removes the non-linear hidden layer and the projection layer is shared for all words. Since all the vectors are averaged, the words are projected into the same position. The skip-gram model maximizes the classification of a word based on another word in the same sentence. Input to a log-linear classifier with continuous projection layer is the current word and predict words within a range before and after the current word. Continuous Bag of Words is faster for larger data sets \cite{MikolovLe}.


\subsection{Skipgram}
Instead of predicting the current word based on the context, skipgram tries to maximize classification of a word based on another word in the same sentence. Using each current word as an input to a log-linear classifier with continuous projection layer, it predicts words within a certain range before and after the current word.

\vspace{-.2cm}
\section{Clustering Methods for Classification}
\label{Clustering}
\vspace{-.2cm}

\subsection{K-Means Clustering}
K-means clustering implements the expectation-maximization function. \\
$J(X,C) = \sum_{i=0}^n min_{\mu_j \in C}(||x_j - \mu_i||^2 $\\
This method of clustering works well for data that is described by spatially separated hyper-spheres. However even though K-means works very well for convex sets, it does not perform well for sets that are non-convex.

\subsection{Spectral Clustering}
Spectral clustering uses the top eigenvectors of a matrix derived from the distance between data points \cite{Spectral Clustering}. This algorithm creates a similarity graph of data points. The graph is then partitioned into disconnected components. For two sets A and B that not necessarily disjoint and are in V, $W(A,B) := \sum_{i \in A, j \in B} w_{ij}$. The minicut implementation involves choosing a partition $A_1, A_2, ..., A_k$ that minimizes $cut(A_1, A_2, ...,A_k) := \frac{1}{2} \sum_{i=1}^k W(A_i, \bar{A}_i)$. \\

Spectral clustering performs well on sparse data and for a small number of clusters, but outliers have significant effect on the results. \\
The algorithm for spectral clustering is that given a set of points $S={s_1, ..., s_n}$ in $\mathbb{R}^l$ that is to be clustered into k subsets:\\
1. Form the affinity matrix $A \in \mathbb{R}^{nxn}$ defined by $A_{ij} = exp(-||s_i - s_j||^2 /2 \sigma^2)$ if $i \neq j$, and $A_ii = 0$.\\
2. Define $D$ to be the diagonal matrix whose $(i,i)$-element is the sum of $A's i-th$ row, and construct the matrix $L = D^{-1/2} AD^{-1/2} $.\\
3. Find $x_1, ..., x_k$, the $k$ largest eigenvectors of $L$ (chosen to be the orthogonal to each other in the case of repeated eigenvalues), and form the matrix $X=[x_1x_2...x_k] \in \mathbb{R}^{nxk}$ by stacking the eigenvectors in columns.\\
4. Form the matrix $Y$ from X by renormalizing each of $X$'s rows to have unit length (i.e. $Y_{ij} = X_{ij}/(\sum_j X_{ij}^2) ^{1/2}$.\\
5. Treating each row of $Y$ as a point in $ \mathbb{R}^k $, cluster them into k clusters via K-means or any other algorithm that attempts to minimize distortion.\\
6. Finally, assign the original point $s_i$ to cluster $j$ if and only if row $i$ of the matrix $Y$ was assigned to cluster $j$.\\
The scaling parameter $\sigma^2$ controls how fast the affinity $A_{ij}$ falls with the distance between $s_i$ and $s_j$. Mapping the points to $ \mathbb{R}^k$, the natural clusters that are non-convex in $\mathbb{R}^2$ form tight clusters \cite{Spectral Clustering}.

\subsection{Hierarchical Clustering}
The type of hierarchical clustering implemented in this research was agglomerative. Each data point starts in its own cluster and as they move up the hierarchy, they merge with their closest neighbors. This clustering algorithm has a tree-like structure. Although agglomerative hierarchical clustering has a high time complexity of $O(n^3)$, it performs very well for non-convex data.\\
For $n$ objects ${1,...,n}$, there is a sequence of $m+l$ clusterings, $C_0, C_1, ..., C_m$, where $C_0$ is the weak clustering and $C_m$ is the strong clustering. Each of these clusterings has a value of $ \alpha_i$, which has the property $\alpha_{j-1} \leq \alpha_j$, for $j=1,...,m$. Because the clusters are merging, they must hold the property $C_{j-1} < C_j$ so that clusters $C_j$ merges with $C_{j-1}$ \cite{Hierarchical}.\\

The agglomerative algorithm is as follows \cite{Hierarchical Clustering}:\\
For $D$ as a $NxN$ matrix, $[d(i,j)]$, clusterings are assigned sequence numbers $0,1,...n-1$. $L(k)$ is the level of the kth clustering and $d[(r),(s)]$ is the proximity between clusters (r) and (s).\\

1. The disjoint clusterings have $L(0) = 0$ and sequence number $m = 0$.\\
2. Find the least dissimilar pair of clusters in the current clustering (e.g. (r) and (s)) according to $d[(r),(s)] = min d[(i),(j)]$, where the minimum is over all the pairs of clusters in the current clustering.\\
3. Increment sequence number $m$ by 1. Merge (r) and (s) into a single cluster to form the next clustering $m$ and set $L(m) to d[(r),(s)]$.\\
4. Update the proximity matrix, D, by deleting the rows and columns corresponding to the clusters (r) and (s) and adding a row and a column corresponding to the newly formed cluster. Proximity between new cluster $(r,s)$ and old cluster $(k)=d[(k),(r,s)] = min d[(k),(r)], d[(k),(s)]$.\\
5. If all objects are in one cluster, stop. Else, go to step 2.\\

\vspace{-.2cm}
\section{Experiments}
\label{StateDependence}
\vspace{-.2cm}

A way to evaluate the clusters generated from the word vectors is to use them in a text classification task. Clustering of word vectors has been used to reduce the feature space in text classification \cite{Baker}, which reduces the time taken to classify text at little cost to F1 score, but no analysis has been done regarding different ways of clustering the text vectors. 

\subsection{Rand Index}

Rand index is a measures of the similarity between two data clusterings. It ranges from $0$ to $1$, where the lower value denotes that the two data clusters don’t agree on any pair of points, whereas the higher value denotes that the two data clusters are exactly the same. It is calculated as follows, given two cluster sets X and Y :\\
Given : \\
$a =$ number of pairs of elements that are in the same cluster in X and in the same cluster in Y\\
$b =$ number of pairs of elements that are in different clusters in X and on different clusters in Y\\
$c =$ number of pairs of elements that are in the same cluster in X and in different clusters in Y\\
$d =$ number of pairs of elements that are in different clusters in X and in the same cluster in Y\\
\\ The rand index is given by $RI = \frac{a+b}{a+b+c+d}$\\
The adjusted Rand Index is adjusted for the chance of element groupings, and may be less than $0$, indicating that the rand index is less than the expected index. It is given by :\\
$ARI = \frac{Index - expectedIndex}{maxIndex - expectedIndex}$

The adjusted rand index was calculated as a first measure of the difference between the clusters. The results of calculating the adjusted rand index are given in Figure 1.

\begin{figure}[hbtp]
\caption{Adjusted Rand Index values}
\centering
\includegraphics[scale=0.4]{../Pictures/ri.PNG}
\end{figure}

Rand index is low for all the cases, indicating that the clusters don’t agree at all. This only tells us that the clusters are extremely different, and nothing about how well formed they are. The extremely low rand index indicates that the clusters do not capture well the semantics of the English language. This could also be due to the nature of the data - a vocabulary of 70k is not very high, there is a large amount of proper nouns (the data is extracts from Wikipedia), and no normalization has been done on the text (lemmatization or stemming).


\subsection{Data for classification}

The dataset used for text classification is the 20 Newsgroups dataset, which is a collection of approximately 20,000 newsgroup documents, partitioned almost evenly across 20 different categories, and commonly used in experiments related to text classification. \\The categories are :\\
'alt.atheism', 'comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware', 'comp.windows.x', 'misc.forsale', 'rec.autos', 'rec.motorcycles', 'rec.sport.baseball', 'rec.sport.hockey', 'sci.crypt', 'sci.electronics', 'sci.med', 'sci.space', 'soc.religion.christian', 'talk.politics.guns', 'talk.politics.mideast', 'talk.politics.misc', 'talk.religion.misc'\\
\\A 1:2 testing/training ratio was used, with 11314 documents (13.782MB) in the training set and 7532 documents (8.262MB) in the test set. The classification was evaluated by F1 score. A naive method was used to form the features for the documents based on the frequency of words in the document in the different clusters. \\
\\The feature matrix $F$ formed had elements given by  :\\
$ F_{ij} = n_{ij}/ \sum_j n_{ij}$ \\where $n_{ij}$ designates the number of words in cluster $j$ in document $i$.\\
\\A major issue with testing over large amounts of data was availability of resources. All the experiments were run over Amazon EC2 instances, and we were limited by both cost and time. Although the generation of the word vectors did not take as much time or memory, the hierarchical and spectral clustering methods in Python were computationally quite expensive to implement.\\
\\Hence, clusters were created using word vectors from 5 MB, 10 MB, and 100 MB data.

\subsection{Methodology}

Various numbers of clusters were produced using k-means clustering, hierarchical clustering, spectral clustering and a hybrid clustering methodology. The hybrid approach consisted of first generating 5000 clusters via k-means and then using the cluster centers to form 500 clusters using hierarchical clustering. This approach was adopted as an attempt to measure the performance of a non-convex clustering methodology over a large amount of data, given the computational complexity of hierarchical clustering.

Results are summarized in Figure 2. The results reported are for classification using linear support vector classification. A sample output of the classification using other classification techniques is shown in Figure 3 and Figure 4. 

\subsection{Results}

The best classification results over all runs are given by k-means clustering using word vectors from 100 MB of data and clustered into 500 clusters. Comparing the results with the same amount of data, at 5 MB the best results are given by spectral clustering using a higher number of clusters. With 10 MB data, better results are seen from hierarchical clustering than from k-means clustering using 100 clusters. Spectral clustering could not be performed on larger data due to computational issues.

\begin{figure}[hbtp]
\caption{Results of classification - the time is given in seconds}
\centering
\includegraphics[scale=0.4]{../Pictures/Capture.PNG}
\end{figure}

For 100 MB data with 500 clusters, the classification results are slightly better for k-means clustering than for hybrid clustering. Hybrid clustering gave an F1 score of 41.67, with the time taken being 158.2 seconds.

\begin{figure}[hbtp]
\caption{Classification results for 10 MB data with 100 clusters using k-means clustering, using multiple classification algorithms. The best results are seen by Linear SVC, although time taken by it is the longest.}
\centering
\includegraphics[scale=0.45]{../Pictures/Capture1.PNG}
\end{figure}

\begin{figure}[hbtp]
\caption{Classification results for 10 MB data with 100 clusters using hierarchical clustering, using multiple classification algorithms. Results are comparable to k-means clusters, but slightly better. }
\centering
\includegraphics[scale=0.45]{../Pictures/Capture2.PNG}
\end{figure}


The clustering algorithms themselves took multiple hours to complete on Amazon EC2 virtual machines. Some more graphs of results generated are given in the appendix.



\subsection{Conclusions and discussion}

The results indicate quite poor classification performance using these word vectors. Performance using word occurrence features for this dataset using a tf-idf metric give up to 82.0 F1 score, although the time taken was 13 seconds.

The results indicate that a higher number of clusters leads to better classification. This indicates that increasing the number of features betters classification, which is to be expected given the complexity of the feature space. It could also indicate that the clusters are well formed enough to provide robust features for the classification.

The results also indicate that a higher amount of data results in a better classification score. This is to be expected, given the nature of both the data used to form the vectors (extracts from Wikipedia) as well as the documents in the 20 Newsgroups dataset. The data is specialised in that it concerns very specific topics, and includes a very large amount of named entities and rare words. The semantic properties of the English language would be much better approximated by vectors formed over much larger data. With a lower amount of data, Euclidean distance as well as cosine distance would be lower between vectors corresponding to words that simply occur in the same context, rather than semantically similar words. The trend indicates that simply using a high enough amount of data would lead to better classification by accurately capturing semantic information about the English language.

The results indicate that time taken by the classification is indeed significantly lower by reducing the feature space (classification using word-occurrence features took 13 seconds to complete).

Importantly, most of the results indicate that clustering using hierarchical or spectral clustering improves the classification results the higher the amount of data and clusters we use. This trend indicates that the vector space is indeed non-convex and k-means clustering might produce ‘worse’ clusters that do not capture semantic information as well as the other clustering methods. The one result that contradicts this is hybrid clustering using 100 MB with 500 clusters, which performs slightly worse than k-means using the same data. Our speculation is that the initial 5000 clusters formed using k-means in the former method were poorly-formed, and hence performing hierarchical clustering over the cluster centers gave poorer results. However, this is hard to confirm unless more experiments can be done using larger data.

Many improvements can be effectively made to improve the performance of the classification. One observation regarding the data was that although the word vectors generated by word2vec are not sparse, the feature vectors generated using the clusters are quite sparse. A method like sparse PCA might have improved the performance. The features could also have been better constructed using a tf-idf metric similar to word-occurrence features. An even better metric would have been to generate word vectors from the 20 Newsgroups and use their distances from the cluster centers. This may have better captured semantic information. Natural languguage processing methodologies like stemming or lemmatizing the words before generating the word vectors and document features would have resulted in a less sparse feature matrix, and could have led to an increase in the F1 score. 

Most importantly, we came to the conclusion that semantic information about the English language can only be accurately captured over a very large amount of training data. Given access to better resources, we may have seen a much better result over the classification task and been able to draw better conclusions regarding the different clustering methodologies used. 
 
\bibliographystyle{plain}
\bibliography{filter}

\newpage
\section{Appendix}

\begin{figure}[hbtp]
\caption{Classification results for 5 MB data with 100 clusters using k-means clustering}
\centering
\includegraphics[scale=0.45]{../Pictures/knn_5mb_100c.PNG}
\end{figure}

\begin{figure}[hbtp]
\caption{Classification results for 5 MB data with 100 clusters using spectral clustering. Results are slightly better than k-means clusters, but time taken is longer. }
\centering
\includegraphics[scale=0.45]{../Pictures/spec_5mb_100c.PNG}
\end{figure}

\begin{figure}[hbtp]
\caption{Classification results for 50 MB data using hybrid clustering}
\centering
\includegraphics[scale=0.45]{../Pictures/kmeans_hier_50mb_500c.png}
\end{figure}

\begin{figure}[hbtp]
\caption{Classification results for 100 MB data using hybrid clustering}
\centering
\includegraphics[scale=0.45]{../Pictures/kmeans_hier_100mb_500c.png}
\end{figure}

\end{document}